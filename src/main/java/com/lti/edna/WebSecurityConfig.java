package com.lti.edna;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.lti.edna.service.MongoUserDetailsService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	private static final Logger LOGGER = LogManager.getLogger(WebSecurityConfig.class);
	@Autowired
	MongoUserDetailsService userService;

	@Override
	public void configure(AuthenticationManagerBuilder builder) throws Exception {
		builder.userDetailsService(userService);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		LOGGER.debug("debug level log message from WebSecurityConfig class begining  ");
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		final CorsConfiguration config = new CorsConfiguration();
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.setAllowCredentials(true);
		config.setAllowedMethods(Arrays.asList("GET", "POST", "DELETE", "OPTIONS"));
		source.registerCorsConfiguration("/**", config);
		http.addFilterBefore(new CorsFilter(), ChannelProcessingFilter.class);
		http.authorizeRequests().antMatchers("/index.html", "/", "/home", "/login","/product/getProducts","/users/users","/users/createUser","/edna-poc/*").permitAll().anyRequest()
				.fullyAuthenticated().and().httpBasic().and().csrf().disable();
		http.cors();
		
		LOGGER.debug("debug level log message from WebSecurityConfig class ending  ");
	}

	@Bean
	public PasswordEncoder passwordEncoder() {

		return new BCryptPasswordEncoder();

	}

}
