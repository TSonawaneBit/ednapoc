package com.lti.edna;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.filter.OncePerRequestFilter;

public class CorsFilter extends OncePerRequestFilter {
	private static final Logger LOGGER = LogManager.getLogger(CorsFilter.class);
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		LOGGER.debug("debug level log message from CorsFilter class and doFilterInternal begining ");
		try {
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
			response.setHeader("Access-Control-Allow-Headers",
					"Access-Control-Allow-Origin, Access-Control-Allow-Methods, Access-Control-Allow-Headers, authorization, Content-type");
			 if ("OPTIONS".equals(request.getMethod())) {
		            response.setStatus(HttpServletResponse.SC_OK);
		        } else {
		            filterChain.doFilter(request, response);
		        }

		} catch (Exception e) {
			e.printStackTrace();

		}

	}
}