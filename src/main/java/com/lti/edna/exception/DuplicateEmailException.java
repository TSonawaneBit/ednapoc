package com.lti.edna.exception;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class DuplicateEmailException extends Exception {
	
	private String code;
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	private static final long serialVersionUID = -5969059934447225115L;

	public DuplicateEmailException(String message,String code) {
		super(message);
		this.code = code;
	}
}
