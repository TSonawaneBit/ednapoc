package com.lti.edna.config;

public interface Constants {

		//HTTP codes
		String SUCCESS_CODE = "200";
		String ERROR_CODE = "500";
		String NOT_FOUND = "404";
		String CONFLICT= "409";

		//Messages
		String ERROR_MESSAGE = "ERROR";
		String HIBERNATE_ERROR_MESSAGE = "HIBERNATE ERROR";
		String SUCCESS = "success";
		String SUCCESS_MESSAGE = "Added Successfully.";
		String SUCCESS_UPDATED = "Updated Successfully.";
		String SUCCESS_DELETED = "Deleted Successfully.";
		String ERROR = "error";
		String ALREADY_EXISTS = "Already Exists.";
		
	
}
