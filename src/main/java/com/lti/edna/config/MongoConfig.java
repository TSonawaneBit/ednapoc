package com.lti.edna.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.lti.edna.EDnaPocApplication;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;

@EnableMongoRepositories(basePackages = "com.lti.edna.repository")
@Configuration
public class MongoConfig {

	private static final Logger LOGGER = LogManager.getLogger(MongoConfig.class);
	MongoClientSettings settings;
	MongoClient mongoClient;

	String connectionURL = "mongodb+srv://admin:root@biddingcluster-2uxii.mongodb.net/eDNAdb";

	public MongoConfig() {
		LOGGER.debug("debug level log message from MongoConfig class and MongoConfig() begining ");
		ConnectionString connectionString = new ConnectionString(connectionURL);
		settings = MongoClientSettings.builder().applyConnectionString(connectionString).retryWrites(true).build();
		LOGGER.debug("debug level log message from MongoConfig class and MongoConfig() ending ");
	}

	@Bean
	public MongoClient mongoClient() {
		LOGGER.debug("debug level log message from MongoConfig class and mongoClient() begining ");
		mongoClient = (MongoClient) MongoClients.create(settings);
		LOGGER.debug("debug level log message from MongoConfig class and mongoClient() ending ");
		return mongoClient;
	}

	@Bean
	public MongoTemplate mongoTemplate() throws Exception {
		LOGGER.debug("Info level log message from MongoConfig class and mongoTemplate() begining ");
		LOGGER.debug("Info level log message from MongoConfig class and mongoTemplate() ending ");		
		return new MongoTemplate(mongoClient(), "eDNAdb");
	}
	
	@Bean
	public MongoDatabase getDatabase() {
		LOGGER.debug("debug level log message from MongoConfig class and getDatabase() begining ");
		MongoDatabase database=mongoClient.getDatabase("eDNAdb");
		LOGGER.debug("debug level log message from MongoConfig class and getDatabase() ending ");
		return database;
	}
	
}