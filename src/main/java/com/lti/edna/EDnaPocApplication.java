package com.lti.edna;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class EDnaPocApplication{

	private static final Logger LOGGER = LogManager.getLogger(EDnaPocApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(EDnaPocApplication.class, args);
		LOGGER.info("Info level log message from EDnaPocApplica  tion class begining ");
		LOGGER.debug("debug level log message from EDnaPocApplication class begining ");

	}

}