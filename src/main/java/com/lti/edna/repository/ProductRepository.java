package com.lti.edna.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.lti.edna.model.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, Integer> {

}
