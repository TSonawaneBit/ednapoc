package com.lti.edna.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.lti.edna.model.User;



@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
	
	@Query("FROM User u where u.address=:addrs")
	List<User> findByAddress(String addrs);
	
	@Query("FROM User u where u.city=:city")
	List<User> findByCity(String city);
	
	@Query("FROM User u where u.category=:category")
	List<User> findByCategory(String category);

}
