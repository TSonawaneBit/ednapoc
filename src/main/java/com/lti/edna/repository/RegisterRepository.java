package com.lti.edna.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lti.edna.model.User;
@Repository
public interface RegisterRepository extends JpaRepository<User, Integer>{

	//Optional<User> findById(Integer userId);



}
