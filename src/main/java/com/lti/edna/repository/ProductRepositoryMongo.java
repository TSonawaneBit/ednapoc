package com.lti.edna.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.lti.edna.model.ProductMongo;

public interface ProductRepositoryMongo extends MongoRepository<ProductMongo, Integer> {
	 @Query("{ 'productTitle' : { '$regex' : ?0 , $options: 'i'}}")
	 List<ProductMongo> findByProductTitle(String productTitle);
	 List<ProductMongo> findByProductId(String productId);
}
