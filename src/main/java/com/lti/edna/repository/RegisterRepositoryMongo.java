package com.lti.edna.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.lti.edna.model.UserMongo;
@Repository
public interface RegisterRepositoryMongo  extends MongoRepository<UserMongo, Integer>{

	List<UserMongo> findByUserName(String userName);



}
