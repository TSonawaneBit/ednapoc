package com.lti.edna.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.lti.edna.model.UserMongo;


@Repository
public interface UsersRepository extends MongoRepository<UserMongo, String> {
	
	UserMongo findByUserName(String userName);
}
