package com.lti.edna.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.lti.edna.model.OrderMongo;

public interface OrderMongoRepository extends MongoRepository<OrderMongo, String> {
    
	
}