package com.lti.edna.model;

import javax.persistence.Id;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;


@Document(collection= "USER_TABLE")
public class UserMongo {
	@Id
	private ObjectId _id;
	/*
	 * @Field(name = "userId") private Integer userId;
	 */
	@Field(name = "userName")
	private String userName;
	@Field(name = "address")
	private String address;
	@Field(name = "city")
	private String city;
	@Field(name = "category")
	private String category;
	@Field(name = "password")
	private String password;
	public ObjectId get_id() {
		return _id;
	}
	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	/*
	 * public Integer getUserId() { return userId; } public void setUserId(Integer
	 * userId) { this.userId = userId; }
	 */
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public UserMongo() {
		super();
		
	}
	public UserMongo(ObjectId _id, String userName, String address, String city, String category,
			String password) {
		super();
		this._id = _id;
//		this.userId = userId;
		this.userName = userName;
		this.address = address;
		this.city = city;
		this.category = category;
		this.password = password;
	}
	@Override
	public String toString() {
		return "UserMongo [_id=" + _id + ", userName=" + userName + ", address=" + address
				+ ", city=" + city + ", category=" + category + ", password=" + password + "]";
	}
	
	

	
	

}
