package com.lti.edna.model;

import javax.persistence.Id;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "ORDER_TABLE")
public class OrderMongo {

	@Id
	private ObjectId _id;

	@Field(name = "userId")
	private String userId;

	@Field(name = "productId")
	private String productId;

	@Field(name = "quantity")
	private int quantity;

	public OrderMongo() {
	}

	public OrderMongo(String userId, String productId, int quantity) {
		super();
		this.userId = userId;
		this.productId = productId;
		this.quantity = quantity;
	}

	public ObjectId get_id() {
		return _id;
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	
	@Override
	public String toString() {
		return "OrderMongo [userId=" + userId + ", productId=" + productId + ", quantity=" + quantity + "]";
	}

}
