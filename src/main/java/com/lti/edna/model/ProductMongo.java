package com.lti.edna.model;

import javax.persistence.Column;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;

@Document(collection = "PRODUCT_TABLE")
public class ProductMongo {

	@Id
	private ObjectId _id;
	@Field("productId")
	@Column(unique=true)
	private String productId;
	@Field("productTitle")
	private String productTitle;
	@Field("productQuantity")
	private Integer productQuantity;
	@Field("productDesc")
	private String productDesc;
	//@Field("productPrice")
	@JsonProperty("productPrice")
	@JsonAlias("productprice")
	private String productPrice;
	@Field("productImg")
	private String productImg;
	public ProductMongo() {
		// TODO Auto-generated constructor stub
	}

	public ProductMongo(ObjectId _id, String productId, String productTitle, Integer productQuantity,
			String productDesc, String productPrice, String productImg) {
		super();
		this._id = _id;
		this.productId = productId;
		this.productTitle = productTitle;
		this.productQuantity = productQuantity;
		this.productDesc = productDesc;
		this.productPrice = productPrice;
		this.productImg = productImg;
	}

	public ObjectId get_id() {
		return _id;
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}

	public Integer getProductQuantity() {
		return productQuantity;
	}

	public void setProductQuantity(Integer productQuantity) {
		this.productQuantity = productQuantity;
	}

	public String getProductDesc() {
		return productDesc;
	}

	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}
	@JsonProperty("productPrice")
	public String getProductprice() {
		return productPrice;
	}
	@JsonProperty("productPrice")
	public void setProductprice(String productPrice) {
		this.productPrice = productPrice;
	}

	public String getProductImg() {
		return productImg;
	}

	public void setProductImg(String productImg) {
		this.productImg = productImg;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_id == null) ? 0 : _id.hashCode());
		result = prime * result + ((productDesc == null) ? 0 : productDesc.hashCode());
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		result = prime * result + ((productImg == null) ? 0 : productImg.hashCode());
		result = prime * result + ((productQuantity == null) ? 0 : productQuantity.hashCode());
		result = prime * result + ((productTitle == null) ? 0 : productTitle.hashCode());
		result = prime * result + ((productPrice == null) ? 0 : productPrice.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductMongo other = (ProductMongo) obj;
		if (_id == null) {
			if (other._id != null)
				return false;
		} else if (!_id.equals(other._id))
			return false;
		if (productDesc == null) {
			if (other.productDesc != null)
				return false;
		} else if (!productDesc.equals(other.productDesc))
			return false;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		if (productImg == null) {
			if (other.productImg != null)
				return false;
		} else if (!productImg.equals(other.productImg))
			return false;
		if (productQuantity == null) {
			if (other.productQuantity != null)
				return false;
		} else if (!productQuantity.equals(other.productQuantity))
			return false;
		if (productTitle == null) {
			if (other.productTitle != null)
				return false;
		} else if (!productTitle.equals(other.productTitle))
			return false;
		if (productPrice == null) {
			if (other.productPrice != null)
				return false;
		} else if (!productPrice.equals(other.productPrice))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProductMongo [_id=" + _id + ", productId=" + productId + ", productTitle=" + productTitle
				+ ", productQuantity=" + productQuantity + ", productDesc=" + productDesc + ", productPrice="
				+ productPrice + ", productImg=" + productImg + "]";
	}

}