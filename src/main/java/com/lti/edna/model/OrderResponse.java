package com.lti.edna.model;

public class OrderResponse {

	private String status;
	private String productId;
	private String orderId;
	private String message;
	private Integer quantity;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	
	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "OrderResponse [status=" + status + ", productId=" + productId + ", orderId=" + orderId + ", message="
				+ message + ", quantity=" + quantity + "]";
	}

	

}
