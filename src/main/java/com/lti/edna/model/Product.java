package com.lti.edna.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int productId;
	private String productDesc;
	private int productPrice;
	private byte productImg;

	public Product() {
		// TODO Auto-generated constructor stub
	}

	public Product(int productId, String productDesc, int productPrice, byte productImg) {
		this.productId = productId;
		this.productDesc = productDesc;
		this.productPrice = productPrice;
		this.productImg = productImg;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductTitel(int productId) {
		this.productId = productId;
	}

	public String getProductDesc() {
		return productDesc;
	}

	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}

	public int getproductPrice() {
		return productPrice;
	}

	public void setproductPrice(int productprice) {
		this.productPrice = productPrice;
	}

	public byte getProductImg() {
		return productImg;
	}

	public void setProductImg(byte productImg) {
		this.productImg = productImg;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((productDesc == null) ? 0 : productDesc.hashCode());
		result = prime * result + productId;
		result = prime * result + productImg;
		result = prime * result + productPrice;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (productDesc == null) {
			if (other.productDesc != null)
				return false;
		} else if (!productDesc.equals(other.productDesc))
			return false;
		if (productId != other.productId)
			return false;
		if (productImg != other.productImg)
			return false;
		if (productPrice != other.productPrice)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productDesc=" + productDesc + ", productprice=" + productPrice
				+ ", productImg=" + productImg + "]";
	}	
}