package com.lti.edna.model;

public class ProductMongoResponse {

	private String status;
	private String productId;
	private String message;
	private Integer productQuantity;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getProductQuantity() {
		return productQuantity;
	}

	public void setQuantity(Integer productQuantity) {
		this.productQuantity = productQuantity;
	}

	@Override
	public String toString() {
		return "ProductMongoResponse [status=" + status + ", productId=" + productId + ", message=" + message
				+ ", productQuantity=" + productQuantity + "]";
	}

}