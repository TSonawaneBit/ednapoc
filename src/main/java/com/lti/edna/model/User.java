
package com.lti.edna.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="USER_TABLE")
public class User {

	private int userId;
	private String userName;
	private String address;
	private String city;
	private String category;
	
	
	public User() {
		super();
		
	}
	public User(int userId, String userName, String address, String category,String city) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.address = address;
		this.category = category;
		this.city = city;
	}

	@Id   
	@GeneratedValue(strategy= GenerationType.AUTO,generator="native"  )
	@GenericGenerator(name = "native", strategy = "native"  )
	@Column(name = "USER_ID", nullable = false)
	public int getUserId() {
		return userId;
	}


	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Column(name = "USER_NAME", nullable = false)
	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "ADDRESS", nullable = false)
	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "CATEGORY", nullable = false)
	public String getCategory() {
		return category;
	}

	
	public void setCategory(String category) {
		this.category = category;
	}

	@Column(name = "CITY", nullable = false)
	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", userName=" + userName + ", address=" + address + ", city=" + city
				+ ", category=" + category + "]";
	}

	

}
