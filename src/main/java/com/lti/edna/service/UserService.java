package com.lti.edna.service;

import java.util.List;

import com.lti.edna.model.User;



public interface UserService {
	
	public List<User> getAllUsers();
	public List<User> findByCity(String city);
	public List<User> findByCategory(String  category);
	

}
