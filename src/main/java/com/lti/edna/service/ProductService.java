package com.lti.edna.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lti.edna.model.Product;
import com.lti.edna.repository.ProductRepository;

@Service
public class ProductService {	
	
	private static final Logger LOGGER = LogManager.getLogger(ProductService.class);
	  @Autowired private ProductRepository productRepository;
	  
	  //getting all Buyer product by using the method findaAll() of CrudRepository  
	  public List<Product> getAllBuyerProducts() { 
		  LOGGER.debug("debug level log message from ProductService class and getAllBuyerProducts() begining");
		  List<Product> buyer = new ArrayList<Product>(); 
		  productRepository.findAll().forEach(buyer1 ->
		  									  buyer.add(buyer1)); 
		  LOGGER.debug("debug level log message from ProductService class and getAllBuyerProducts() end");
		  return buyer; 
	  }
	  
	//getting a specific record by using the method findById() of CrudRepository  
	  public Product getBuyerProductById(int id) {
		  LOGGER.debug("debug level log message from ProductService class and getAllBuyerProducts() begining and end");
		  return productRepository.findById(id).get(); 
	  }
	  
	//saving a specific record by using the method save() of CrudRepository  
	  public void saveOrUpdate(Product buyer)   
	  {  
		  LOGGER.debug("debug level log message from ProductService class and saveOrUpdate() begining and end");
		  productRepository.save(buyer);  
	  }  
	  //deleting a specific record by using the method deleteById() of CrudRepository  
	  public void delete(int id)   
	  {  
		  LOGGER.debug("debug level log message from ProductService class and delete() begining and end");
		  productRepository.deleteById(id);  
	  }  
	  //updating a record  
	  public void update(Product buyer, int productid)   
	  {  
		  LOGGER.debug("debug level log message from ProductService class and update() begining and end");
		  productRepository.save(buyer);  
	  }  
}