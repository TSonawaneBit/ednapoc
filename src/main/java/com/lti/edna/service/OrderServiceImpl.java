package com.lti.edna.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.lti.edna.model.Order;
import com.lti.edna.model.OrderMongo;
import com.lti.edna.model.OrderResponse;
import com.lti.edna.repository.OrderMongoRepository;
import com.lti.edna.repository.OrderRepository;

@Component
public class OrderServiceImpl implements OrderService {

	private static final Logger LOGGER = LogManager.getLogger(OrderServiceImpl.class);
	@Autowired
	OrderRepository orderDao;

	@Autowired
	OrderMongoRepository orderMongoDao;

	@Autowired
	MongoTemplate mongoTemplate;

	@Override
	public OrderResponse placeOrder(Order orderObj) {
		LOGGER.debug("debug level log message from OrderServiceImpl class and placeOrder() begining ");
		LOGGER.debug("debug Order for user ID   "+ orderObj.getUserId());
		//System.out.println("Placing Order for user ID :: " + orderObj.getUserId());
		Order order = orderDao.save(orderObj);
		OrderResponse response = new OrderResponse();
		if (null != order && order.getUserId() == orderObj.getUserId()) {
			response.setMessage("Order placed successfully");
			response.setProductId(order.getProductId());
			response.setOrderId("001");
			response.setStatus("success");
		} else {
			response.setMessage("Order placing failed - Review your order or try again");
			response.setProductId(order.getProductId());
			response.setOrderId("001");
			response.setStatus("failure");
		}
		LOGGER.debug("debug level log message from OrderServiceImpl class and placeOrder() begining ");
		return response;

	}

	@Override
	public OrderResponse placeOrderMongo(OrderMongo orderObj) {
		
		LOGGER.debug("debug level log message from OrderServiceImpl class and placeOrderMongo method begining ");
		LOGGER.debug("debug Order for user ID :: " + orderObj.getUserId());
		LOGGER.debug("debug selected :: " + orderObj.getProductId());
		//System.out.println("Placing Order for user ID :: " + orderObj.getUserId());
		//System.out.println("Product selected :: " + orderObj.getProductId());

		OrderMongo ordermongo = orderMongoDao.save(orderObj);
		//System.out.println("SAVING RECORD :: " + orderObj.toString());
		LOGGER.debug("SAVING RECORD :: " + orderObj.toString());

		OrderResponse response = new OrderResponse();
		if (null != ordermongo && ordermongo.getUserId() == orderObj.getUserId()) {
			response.setMessage("Order placed successfully. Order ID is - " + ordermongo.get_id().toString());
			response.setProductId(ordermongo.getProductId());
			response.setOrderId(ordermongo.get_id().toString());
			response.setQuantity(ordermongo.getQuantity());
			response.setStatus("success");
		} else {
			response.setMessage("Order placing failed - Review your order or try again");
			response.setProductId(orderObj.getProductId());
			response.setOrderId("NA");
			response.setStatus("error");
		}
		LOGGER.debug("debug level log message from OrderServiceImpl class and placeOrderMongo method end ");
		return response;
	}

	@Override
	public List<Order> listOrders() {
		LOGGER.debug("debug level log message from OrderServiceImpl class and listOrders method begining ");
		List<Order> orderList = orderDao.findAll();
		LOGGER.debug("debug level log message from OrderServiceImpl class and listOrders method end ");
		return orderList;
	}

	@Override
	public List<OrderResponse> listOrdersMongo() {
		LOGGER.debug("debug level log message from OrderServiceImpl class and listOrdersMongo method begining ");
		List<OrderMongo> orderList = orderMongoDao.findAll();
		List<OrderResponse> orderResList = new ArrayList<>();
		for (OrderMongo ord : orderList) {
			OrderResponse response = new OrderResponse();
			response.setOrderId(ord.get_id().toString());
			response.setProductId(ord.getProductId());
			response.setQuantity(ord.getQuantity());
			response.setStatus("success");
			orderResList.add(response);
			//System.out.println("PRINTING OBJECT :::" + ord.toString());
			LOGGER.debug("PRINTING OBJECT :::" + ord.toString());
		}
		LOGGER.debug("debug level log message from OrderServiceImpl class and listOrdersMongo method end ");
		return orderResList;
	}

	@Override
	public List<Order> listOrdersbyUserId(Integer userId) {
		LOGGER.debug("debug level log message from OrderServiceImpl class and listOrdersbyUserId method begining ");
		//List<Order> orderList = orderDao.findByUserId(userId);
		List<Order> orderList = new ArrayList<>();
		return orderList;
	}

	@Override
	public List<OrderResponse> listOrdersbyUserIdMongo(String userId) {
		LOGGER.debug("debug level log message from OrderServiceImpl class and listOrdersbyUserId method begining ");
		LOGGER.debug("USERID:::: "+userId);
		//System.out.println("USER:::: "+userId);
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId));
		List<OrderMongo> orderList = mongoTemplate.find(query, OrderMongo.class);
		List<OrderResponse> orderResList = new ArrayList<>();
		for (OrderMongo ord : orderList) {
			OrderResponse response = new OrderResponse();
			response.setOrderId(ord.get_id().toString());
			response.setProductId(ord.getProductId());
			response.setQuantity(ord.getQuantity());
			response.setStatus("success");
			orderResList.add(response);
			//System.out.println("PRINTING ORDER BY USER id " + userId + " >>> " + ord.toString());
			LOGGER.debug("PRINTING ORDER BY USER id " + userId + " >>> " + ord.toString());
		}
		LOGGER.debug("debug level log message from OrderServiceImpl class and listOrdersbyUserId method end ");
		return orderResList;
	}

	@Override
	public OrderResponse updateOrder(OrderMongo order) {
		LOGGER.debug("debug level log message from OrderServiceImpl class and updateOrder method begining ");
		OrderMongo ordermongo = orderMongoDao.save(order);
		OrderResponse response = new OrderResponse();
		if (null != ordermongo && ordermongo.getUserId() == order.getUserId()) {
			response.setMessage("Order updated successfully. Order ID is - " + ordermongo.get_id().toString());
			response.setProductId(ordermongo.getProductId());
			response.setOrderId(ordermongo.get_id().toString());
			response.setStatus("success");
		} else {
			response.setMessage("Order updation failed - Review your order or try again");
			response.setProductId(order.getProductId());
			response.setOrderId("NA");
			response.setStatus("error");
		}
		LOGGER.debug("debug level log message from OrderServiceImpl class and updateOrder method end ");
		return response;
	}

}
