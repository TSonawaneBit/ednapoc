package com.lti.edna.service;

import java.util.List;

import com.lti.edna.model.Order;
import com.lti.edna.model.OrderMongo;
import com.lti.edna.model.OrderResponse;

public interface OrderService {
	
	public OrderResponse placeOrder(Order orderObj);
	
	public OrderResponse placeOrderMongo(OrderMongo orderObj);
	
	public List<Order> listOrders();
	
	public List<OrderResponse> listOrdersMongo();
	
	public List<Order> listOrdersbyUserId(Integer userId);
	
	public List<OrderResponse> listOrdersbyUserIdMongo(String userId); 
	
	public OrderResponse updateOrder(OrderMongo order); 

}
