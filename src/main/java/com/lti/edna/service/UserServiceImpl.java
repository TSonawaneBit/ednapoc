package com.lti.edna.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lti.edna.model.User;
import com.lti.edna.repository.UserRepository;



@Service
public class UserServiceImpl implements UserService {
	
	private static final Logger LOGGER = LogManager.getLogger(UserServiceImpl.class);
	
	@Autowired
	UserRepository userRepository;

	@Override
	public List<User> getAllUsers() {
		LOGGER.debug("debug level log message from UserServiceImpl class and getAllUsers() begining");
		//System.out.println("service starting");
		List<User> allUsers=userRepository.findAll();
		//System.out.println("service ending"+allUsers);
		LOGGER.debug("debug level log message from UserServiceImpl class and getAllUsers() end");
		return allUsers;
	}
	
	@Override
	public List<User> findByCategory(String  category) {
		LOGGER.debug("debug level log message from UserServiceImpl class and findByCategory() begining");
		List<User> categoryList=userRepository.findByCategory(category);
		LOGGER.debug("debug level log message from UserServiceImpl class and findByCategory() end");
		return categoryList;
	}

	@Override
	public List<User> findByCity(String city) {
		LOGGER.debug("debug level log message from UserServiceImpl class and findByCity() begining");
		List<User> cityList=userRepository.findByCity(city);
		LOGGER.debug("debug level log message from UserServiceImpl class and findByCity() end");
		return cityList;
	}

}
