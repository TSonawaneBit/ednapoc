package com.lti.edna.service;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lti.edna.model.ProductMongo;
import com.lti.edna.model.ProductMongoResponse;
import com.lti.edna.repository.ProductRepositoryMongo;

@Service
public class ProductMongoService {

	private static final Logger LOGGER = LogManager.getLogger(ProductMongoService.class);
	@Autowired
	private ProductRepositoryMongo productRepositoryMongo;

	// getting all Buyer product by using the method findaAll() of CrudRepository
	public List<ProductMongo> getAllProducts() {
		LOGGER.debug("debug level log message from ProductMongoService class snd getAllProducts() begining and end ");
		return (List<ProductMongo>) productRepositoryMongo.findAll();
	}

	// getting a specific record by using the method findById() of CrudRepository
	public List<ProductMongo> getProductById(String productId) {
		LOGGER.debug("debug level log message from ProductMongoService class snd getProductById() begining and end ");
		return productRepositoryMongo.findByProductId(productId);
	}

	// getting a specific record by using the method findById() of CrudRepository
	public List<ProductMongo> getProductByTitle(String productTitle) {
		LOGGER.debug("debug level log message from ProductMongoService class snd getProductByTitle() begining and end ");
		return productRepositoryMongo.findByProductTitle(productTitle);
	}

	// saving a specific record by using the method save() of CrudRepository
	public ProductMongoResponse saveOrUpdate(ProductMongo productMongo) {
		LOGGER.debug("debug level log message from ProductMongoService class snd saveOrUpdate() begining  ");
		ProductMongo prodMongo = productRepositoryMongo.save(productMongo);
		ProductMongoResponse response = new ProductMongoResponse();
		if (null != prodMongo && prodMongo.getProductId() == productMongo.getProductId()) {
			response.setMessage("Product "+productMongo.getProductId()+" has been added successfully");
			response.setProductId(productMongo.getProductId());
			response.setQuantity(productMongo.getProductQuantity());
			response.setStatus("success");
		} else {
			response.setMessage("failed to save product");
			response.setProductId(prodMongo.getProductId());
			response.setStatus("failure");
		}
		LOGGER.debug("debug level log message from ProductMongoService class snd saveOrUpdate() end  ");
		return response;
	}

	// deleting a specific record by using the method deleteById() of CrudRepository
	public void delete(Integer id) {
		//productRepositoryMongo.deleteById(id);
		LOGGER.debug("debug level log message from ProductMongoService class snd delete() begining and end ");
		productRepositoryMongo.deleteById(id);
	}

	// delete all
	public void deleteAll() {
		LOGGER.debug("debug level log message from ProductMongoService class snd deleteAll() begining and end ");
		productRepositoryMongo.deleteAll();
	}

	// updating a record
	public void update(ProductMongo productMongo) {
		LOGGER.debug("debug level log message from ProductMongoService class snd update() begining and end ");
		productRepositoryMongo.save(productMongo);
	}
}