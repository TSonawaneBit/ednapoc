package com.lti.edna.service;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.lti.edna.model.UserMongo;
import com.lti.edna.repository.UsersRepository;

@Component
@CrossOrigin(origins = "*")
public class MongoUserDetailsService implements UserDetailsService{
	
	private static final Logger LOGGER = LogManager.getLogger(MongoUserDetailsService.class);
	@Autowired
	UsersRepository repository;
	
	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		LOGGER.debug("debug level log message from MongoUserDetailsService class and loadUserByUsername() begining  " +userName);
		UserMongo user = repository.findByUserName( userName);	
		if(user == null){
	    	//LOGGER.info(" User not found Please try different userId ending  " +user );
	        throw new UsernameNotFoundException("User not found Please try different userId ");
	    }else{
	        
	        List<SimpleGrantedAuthority> authorities = Arrays.asList(new SimpleGrantedAuthority("user"));
		    
	        LOGGER.debug("debug level log message from MongoUserDetailsService class and loadUserByUsername() ending  " +user.getUserName()+ "  "+user.getPassword());
		   	return new User(user.getUserName(), user.getPassword(), authorities);
	    }  
	}
}
