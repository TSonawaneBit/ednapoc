package com.lti.edna.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.lti.edna.exception.ResourceNotFoundException;
import com.lti.edna.model.User;
import com.lti.edna.repository.RegisterRepository;

@RestController
public class RegisterController {
	
	private static final Logger LOGGER = LogManager.getLogger(RegisterController.class);

	@Autowired
	RegisterRepository service;
	
	@GetMapping("/user")
	public List<User> getAllUser() {
		LOGGER.info("Info level log message from RegisterController class and getAllUser() begining ");
		return service.findAll();
	}
	@GetMapping("/user/{id}")
	public ResponseEntity<User> getUserById(@PathVariable(value = "id") Integer userId)
			throws ResourceNotFoundException {
		LOGGER.debug("debug level log message from RegisterController class and getUserById() begining ");
		User user = service.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: " + userId));
		LOGGER.debug("debug level log message from RegisterController class and getUserById() end ");
		return ResponseEntity.ok().body(user);
	}
	
	@PostMapping("/createUser")
	public User createUser(@RequestBody User user) {
		LOGGER.debug("debug level log message from RegisterController class and createUser() begining ");
		return service.save(user);
	}

	@PutMapping("/user/{id}")
	public ResponseEntity<User> updateUser(@PathVariable(value = "id") Integer userId,
			@RequestBody User userDetails) throws ResourceNotFoundException {
		LOGGER.debug("debug level log message from RegisterController class and updateUser() begining ");
		User user = service.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: " + userId));

		user.setUserName(userDetails.getUserName());
		user.setAddress(userDetails.getAddress());
		user.setCity(userDetails.getCity());
		user.setCategory(userDetails.getCategory());
		
		
		final User updatedUser = service.save(user);
		LOGGER.debug("debug level log message from RegisterController class and updateUser() end ");
		return ResponseEntity.ok(updatedUser);
	}

	@DeleteMapping("/user/{id}")
	public Map<String, Boolean> deleteUser(@PathVariable(value = "id") Integer userId)
			throws ResourceNotFoundException {
		LOGGER.debug("debug level log message from RegisterController class and deleteUser() begining ");
		User user = service.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: " + userId));

		service.delete(user);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		LOGGER.debug("debug level log message from RegisterController class and deleteUser() end ");
		return response;
	}


}