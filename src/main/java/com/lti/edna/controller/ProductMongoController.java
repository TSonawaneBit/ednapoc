package com.lti.edna.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lti.edna.model.ProductMongo;
import com.lti.edna.model.ProductMongoResponse;
import com.lti.edna.service.ProductMongoService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/product")
public class ProductMongoController {
	
	private static final Logger LOGGER = LogManager.getLogger(ProductMongoController.class);

	@Autowired
	ProductMongoService productService;

	@GetMapping("/getProducts")
	private List<ProductMongo> getAllProducts() {
		LOGGER.info("Info level log message from ProductMongoController class and getAllProducts()  begining ");
		return productService.getAllProducts();
	}

	@RequestMapping("/getProductMongoById/{productId}")
	private List<ProductMongo> getProductById(@PathVariable("productId") String productId) {
		LOGGER.debug("debug level log message from ProductMongoController class and getProductById()  begining ");
		return productService.getProductById(productId);
	}

	@RequestMapping("/getProductMongo/{productTitle}")
	public List<ProductMongo> getProductByTitle(@PathVariable("productTitle") String productTitle) {
		LOGGER.debug("debug level log message from ProductMongoController class and getProductByTitle()  begining ");
		return productService.getProductByTitle(productTitle);
	}

	// creating a delete mapping that deletes a specified buyerMongo product
	@DeleteMapping("/deleteProductMongo/{productId}")
	private void deleteProduct(@PathVariable("_id") Integer id) {
		LOGGER.debug("debug level log message from ProductMongoController class and deleteProduct()  begining ");
		productService.delete(id);
	}

	// creating post mapping that post the buyerMongo product detail in the database
	@RequestMapping("/addProductMongo")
	private ProductMongoResponse saveProduct(@RequestBody ProductMongo productMongo) {
		LOGGER.debug("debug level log message from ProductMongoController class and deleteProduct()  begining ");
		String productId = "PRD";
		int prodSize = productService.getAllProducts().size();
		productId = productId + prodSize;
		productMongo.setProductId(productId);
		LOGGER.debug("debug level log message get  productMongo  "+productMongo);
		LOGGER.debug("debug level log message Add Product   "+ productMongo.toString());
		//System.out.println("######productMongo:" + productMongo);
		//System.out.println("Add Product :: " + productMongo.toString());
		return productService.saveOrUpdate(productMongo);
	}

	// creating put mapping that updates the buyerMongo detail
	@PutMapping("/updateProductMongo")
	private ProductMongo update(@Valid @RequestBody ProductMongo productMongo) {
		LOGGER.debug("debug level log message from ProductMongoController class and update()  begining ");
		productService.saveOrUpdate(productMongo);
		LOGGER.debug("debug level log message from ProductMongoController class and update()  end ");
		return productMongo;
	}
}