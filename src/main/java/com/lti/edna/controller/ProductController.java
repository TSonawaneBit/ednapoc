package com.lti.edna.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lti.edna.model.Product;
import com.lti.edna.service.ProductService;

@RestController
public class ProductController {

	private static final Logger LOGGER = LogManager.getLogger(ProductController.class);
	@Autowired
	ProductService productService;

	@GetMapping("/buyer")
	private List<Product> getAllBuyerProducts() {
		LOGGER.debug("debug level log message from ProductController class and getAllBuyerProducts() begining ");
		return productService.getAllBuyerProducts();
	}

	@RequestMapping("/getbuyer/{productId}")
	private Product getBuyerProductById(@PathVariable("productId") int productId) {
		LOGGER.debug("debug level log message from ProductController class and getBuyerProductById() begining ");
		return productService.getBuyerProductById(productId);
	}

	//creating a delete mapping that deletes a specified buyer product  
	@DeleteMapping("/deletebuyer/{productId}")
	private void deleteBuyerProduct(@PathVariable("productId") int productId)   
	{  
		LOGGER.debug("debug level log message from ProductController class and deleteBuyerProduct() begining ");
		productService.delete(productId);  
	}  
	
	//creating post mapping that post the buyer product detail in the database 
	@RequestMapping("/addbuyer")
	private int saveBuyer(@RequestBody Product Buyer) {
		LOGGER.debug("debug level log message from ProductController class and saveBuyer() begining ");
		productService.saveOrUpdate(Buyer);
		LOGGER.debug("debug level log message from ProductController class and saveBuyer() end ");
		return Buyer.getProductId();
	}
	
	//creating put mapping that updates the buyer detail   
	@PutMapping("/updatebuyer")  
	private Product update(@RequestBody Product buyer)   
	{  
		LOGGER.debug("debug level log message from ProductController class and update() begining ");
		productService.saveOrUpdate(buyer);  
		LOGGER.debug("debug level log message from ProductController class and update() begining ");
		return buyer;  
	}  
}