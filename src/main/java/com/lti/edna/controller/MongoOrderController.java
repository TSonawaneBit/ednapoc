package com.lti.edna.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.lti.edna.model.OrderMongo;
import com.lti.edna.model.OrderResponse;
import com.lti.edna.service.OrderService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/ednaMongo/v1")
public class MongoOrderController {
	
	private static final Logger LOGGER = LogManager.getLogger(MongoOrderController.class);

	@Autowired
	private OrderService orderService;

	/** Mapping to Place Order **/
	@PostMapping("/placeOrder")
	@ResponseStatus(code= HttpStatus.OK)
	public OrderResponse placeOrder(@Valid @RequestBody OrderMongo orderObj) {
		LOGGER.debug("debug level log message from MongoOrderController class and placeOrder method begining ");
		
		return orderService.placeOrderMongo(orderObj);
	}

	/** Mapping to Fetch All Orders **/
	@GetMapping("/listOrders")
	@ResponseStatus(code= HttpStatus.OK)
	public List<OrderResponse> listOrders() {
		LOGGER.debug("debug level log message from MongoOrderController class and listOrders method begining ");
		
		return orderService.listOrdersMongo();
	}

	/** Mapping to Fetch All orders by userId **/
	@GetMapping("/listOrdersbyUserId/{userId}")
	@ResponseStatus(code= HttpStatus.OK)
	public List<OrderResponse> listOrdersbyUserId(@PathVariable String userId) {
		LOGGER.debug("debug level log message from MongoOrderController class and listOrdersbyUserId method begining ");
		
		return orderService.listOrdersbyUserIdMongo(userId);
	}


	


	@PutMapping("/updateOrder")
	@ResponseStatus(code= HttpStatus.OK)
	private OrderResponse update(@Valid @RequestBody OrderMongo orderMongo) {
		LOGGER.debug("debug level log message from MongoOrderController class and update method begining ");
		OrderResponse orderRes = orderService.updateOrder(orderMongo);
		LOGGER.debug("debug level log message from MongoOrderController class and update method end ");
		return orderRes;
		
	}

}
