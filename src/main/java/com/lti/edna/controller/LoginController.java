package com.lti.edna.controller;

import java.security.Principal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@CrossOrigin(origins = "*")
public class LoginController {	
	private static final Logger LOGGER = LogManager.getLogger(LoginController.class);
	  @RequestMapping("/login")
	  public String user(Principal user) {		
		  LOGGER.debug("debug level log message from LoginController class and user method begining ");
		  String userName  =user.getName();
		  LOGGER.debug("debug level log message from LoginController class and user method end ");
		  return userName;		
	  }
}
