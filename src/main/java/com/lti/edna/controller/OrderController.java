package com.lti.edna.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lti.edna.model.Order;
import com.lti.edna.model.OrderResponse;
import com.lti.edna.service.OrderService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/edna/v1")
public class OrderController {
	private static final Logger LOGGER = LogManager.getLogger(OrderController.class);
	@Autowired
	private OrderService orderService;

	/** Mapping to Place Order **/
	@PostMapping("/placeOrder")
	public OrderResponse placeOrder(@Valid @RequestBody Order orderObj) {
		LOGGER.debug("debug level log message from OrderController class and placeOrder() begining ");
		return orderService.placeOrder(orderObj);
	}

	/** Mapping to Fetch All Orders **/
	@GetMapping("/listOrders")
	public List<Order> listOrders() {
		LOGGER.debug("debug level log message from OrderController class and listOrders() begining ");
		return orderService.listOrders();
	}

	/** Mapping to Fetch All orders by userId **/
	@GetMapping("/listOrdersbyUserId/{userId}")
	public List<Order> listOrdersbyUserId(@PathVariable Integer userId) {
		LOGGER.debug("debug level log message from OrderController class and listOrdersbyUserId() begining ");
		return orderService.listOrdersbyUserId(userId);
	}

}
