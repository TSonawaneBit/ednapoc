
package com.lti.edna.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.lti.edna.dto.UserDTO;
import com.lti.edna.model.User;
import com.lti.edna.service.UserService;


@RestController
public class UserController {
	
	private static final Logger LOGGER = LogManager.getLogger(UserController.class);
	@Autowired
	UserService userService;
	
	@GetMapping(value="/users/all",  produces="application/json")
	public ResponseEntity<List<UserDTO>> getAllUsers(){
		LOGGER.debug("debug level log message from UserController class and getAllUsers() begining ");
		//System.out.println("Controller starting");
		List<User> usersList =userService.getAllUsers();
		List<UserDTO> usersList2 = new  ArrayList<>();
		for(User user : usersList) {
			UserDTO dto = new  UserDTO();
			dto.setUserId(user.getUserId());
			dto.setUserName(user.getUserName());
			dto.setAddress(user.getAddress());
			dto.setCategory(user.getCategory());
			dto.setCity(user.getCity());
			usersList2.add(dto);
			LOGGER.debug("debug level log message from UserController class and getAllUsers() end ");
			//System.out.println("Controller ending");
		}
		return new  ResponseEntity<List<UserDTO>>(usersList2, HttpStatus.OK);		
	}
	
	@GetMapping(value="/users/{city}",  produces="application/json")
	public  List<UserDTO> findUserByCity(@PathVariable   String  city) {
		LOGGER.debug("debug level log message from UserController class and findUserByCity() begining ");
		List<User> cityList11 = userService.findByCity(city);
		List<UserDTO>  cityList=new ArrayList<>();
		for(User cityList1 : cityList11) {
			UserDTO dto=new  UserDTO();
			dto.setUserId(cityList1.getUserId());
			dto.setUserName(cityList1.getUserName());
			dto.setAddress(cityList1.getAddress());
			dto.setCategory(cityList1.getCategory());
			dto.setCity(cityList1.getCity());
			cityList.add(dto);
		}
		LOGGER.debug("debug level log message from UserController class and findUserByCity() end ");
		return cityList;	
	}
	
	@GetMapping(value="/user/{category}",  produces="application/json")
	public  List<UserDTO> findUserByCategory(@PathVariable   String  category) {
		LOGGER.debug("debug level log message from UserController class and findUserByCategory() begining ");
		List<User> categoryList11 = userService.findByCategory(category);
		List<UserDTO>  categoryList=new ArrayList<>();
		for(User category1 : categoryList11) {
			UserDTO dto=new  UserDTO();
			dto.setUserId(category1.getUserId());
			dto.setUserName(category1.getUserName());
			dto.setAddress(category1.getAddress());
			dto.setCategory(category1.getCategory());
			dto.setCity(category1.getCity());
			categoryList.add(dto);
		}
		LOGGER.debug("debug level log message from UserController class and findUserByCategory() end ");
		return categoryList;
	
	}
}
