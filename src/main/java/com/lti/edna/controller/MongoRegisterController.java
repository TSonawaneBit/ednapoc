package com.lti.edna.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lti.edna.config.Constants;
import com.lti.edna.exception.DuplicateEmailException;
import com.lti.edna.model.UserMongo;
import com.lti.edna.repository.RegisterRepositoryMongo;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/users")
public class MongoRegisterController {
	
	private static final Logger LOGGER = LogManager.getLogger(MongoRegisterController.class);

	@Autowired
	RegisterRepositoryMongo service;
	
	@GetMapping("/users")
	public List<UserMongo> getAllUser() {
		LOGGER.debug("debug level log message from MongoRegisterController class and  getAllUser() begining ");
		return service.findAll();
	}
	/*
	 * @GetMapping("/users/{userId}") public Optional<UserMongo>
	 * getUserById(@PathVariable(value = "userId") Integer userId){
	 * 
	 * return service.findById(userId); 
	 * }
	 */
	
	@PostMapping("/createUser")
	public UserMongo createUser(@RequestBody UserMongo user) throws DuplicateEmailException {
		LOGGER.info("Info level log message from MongoRegisterController class and  createUser() begining ");
		try{
			if(user!= null){
				List<UserMongo> existingUser = new ArrayList<>();
				service.findByUserName(user.getUserName()).forEach(existingUser::add);
				LOGGER.debug("debug level log message from get existingUser checking  " +existingUser);
				if(existingUser.size() != 0){
					 throw new DuplicateEmailException("User already exist", Constants.ALREADY_EXISTS);  
				}
				user.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt()));
				return service.save(user);
			}
		}catch(RuntimeException e){
			throw e;
		}
		return user;
	}

	/*
	 * @PutMapping("/users/{userId}") public ResponseEntity<UserMongo>
	 * updateUser(@PathVariable(value = "userId") Integer userId,
	 * 
	 * @RequestBody User userDetails) throws ResourceNotFoundException { UserMongo
	 * user = service.findById(userId) .orElseThrow(() -> new
	 * ResourceNotFoundException("User not found for this id :: " + userId));
	 * 
	 * user.setUserName(userDetails.getUserName());
	 * user.setAddress(userDetails.getAddress());
	 * user.setCity(userDetails.getCity());
	 * user.setCategory(userDetails.getCategory());
	 * 
	 * 
	 * final UserMongo updatedUser = service.save(user); return
	 * ResponseEntity.ok(updatedUser); }
	 * 
	 * @DeleteMapping("/users/{userId}") public Map<String, Boolean>
	 * deleteUser(@PathVariable(value = "userId") Integer userId) throws
	 * ResourceNotFoundException { UserMongo user = service.findById(userId)
	 * .orElseThrow(() -> new
	 * ResourceNotFoundException("User not found for this id :: " + userId));
	 * 
	 * service.delete(user); Map<String, Boolean> response = new HashMap<>();
	 * response.put("deleted", Boolean.TRUE); return response; }
	 */


}
