package com.example.demo;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

@SpringBootTest
public class OrderServiceTests {

	public static final String BASE_URI = "http://localhost:8090/edna/v1";
	public static final String BASE_URI_MONGO = "http://localhost:8090/eDNA/ednaMongo/v1";

	/**** MONGODB related test starts ****/
	@Test
	public void testPlaceOrderMongo() {

		RestAssured.baseURI = BASE_URI_MONGO;
		RequestSpecification request = RestAssured.given();
		JSONObject requestParams = new JSONObject();
		requestParams.put("productId", "Laptop");
		requestParams.put("userId", 2);
		requestParams.put("quantity", 2);
		request.header("Content-Type", "application/json");
		request.body(requestParams.toJSONString());
		Response response = request.post("/placeOrder");
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode, 200);
		String successCode = response.jsonPath().get("status");
		System.out.println(successCode);
		Assert.assertEquals("Correct Success code was returned", successCode, "success");

	}

	@Test
	public void testListOrdersMongo() {
		RestAssured.get(BASE_URI_MONGO + "/listOrders").then().assertThat().statusCode(200);
	}

	@Test
	public void testListOrdersbyUserIdMongo() {
		RestAssured.given().pathParam("userId", 1).when().get(BASE_URI_MONGO + "/listOrdersbyUserId/{userId}").then().assertThat()
				.statusCode(200);
	}
	
	@Test
	public void testsearchOrderMongo() {
		RestAssured.given().pathParam("orderId", "5edf8d6a05c2a90db50c847d").when().get(BASE_URI_MONGO + "/searchOrder/{orderId}").then().assertThat()
				.statusCode(200);
	}
	
	
	
	/**** MYSQL related test starts ****/
	@Test
	public void testPlaceOrder() {

		RestAssured.baseURI = "http://localhost:8090/edna/v1";
		RequestSpecification request = RestAssured.given();
		JSONObject requestParams = new JSONObject();
		requestParams.put("productId", "P01");
		request.header("Content-Type", "application/json");
		request.body(requestParams.toJSONString());
		Response response = request.post("/placeOrder");
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode, 200);
		String successCode = response.jsonPath().get("status");
		System.out.println(successCode);
		Assert.assertEquals("Correct Success code was returned", successCode, "success");

	}

	@Test
	public void testListOrders() {
		RestAssured.get("http://localhost:8090/edna/v1/listOrders").then().assertThat().statusCode(200);
	}

	@Test
	public void testListOrdersbyUserId() {
		RestAssured.given().pathParam("userId", 1).when()
				.get("http://localhost:8090/edna/v1/listOrdersbyUserId/{userId}").then().assertThat().statusCode(200);
	}

}