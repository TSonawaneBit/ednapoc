package com.example.demo;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.lti.edna.model.UserMongo;
import com.lti.edna.repository.RegisterRepositoryMongo;

@SpringBootTest
class EDnaPocApplicationTests {
	
	
	@Autowired
	RegisterRepositoryMongo service;
    @Test
    public void testPlaceOrder() {

    	
    	UserMongo reg = new UserMongo();
        reg.setUserName("ABC");
        reg.setPassword("abc123");
        reg.setCity("Pune");
        reg.setAddress("Pune");
        reg.setCategory("Buyer");
        service.save(reg);

        List<UserMongo> users = service.findAll();
        users.forEach(user->System.out.println(user.getUserName()));
		
		/*
		 * RestAssured.baseURI = "http://localhost:8084/eDNA"; RequestSpecification
		 * request = RestAssured.given(); JSONObject requestParams = new JSONObject();
		 * requestParams.put("userId", "1010"); requestParams.put("userName",
		 * "Chota Bhim"); requestParams.put("address", "India");
		 * requestParams.put("city", "Banglore"); requestParams.put("category",
		 * "Seller"); request.header("Content-Type", "application/json");
		 * request.body(requestParams.toJSONString()); Response response =
		 * request.post("/createUser"); int statusCode = response.getStatusCode();
		 * Assert.assertEquals(statusCode, 200); String successCode =
		 * response.jsonPath().get("status"); System.out.println(successCode);
		 * Assert.assertEquals("Correct Success code was returned", successCode,
		 * "success");
		 */		 
    }



}
