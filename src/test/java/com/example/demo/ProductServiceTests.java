package com.example.demo;

import java.util.HashMap;
import java.util.Map;

import org.bson.types.ObjectId;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.lti.edna.model.ProductMongo;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
@SpringBootTest
public class ProductServiceTests {

	  public static final String BASE_URI = "http://localhost:8080/eDNA/buyer";	
	  @Test public void testAddProduct() {	 
		  
			  RestAssured.baseURI = "http://localhost:8080/eDNA/buyer";
			  System.out.println(RestAssured.baseURI); RequestSpecification request =
			  RestAssured.given(); JSONObject requestParams = new JSONObject(); ProductMongo
			  buyerMongo=new ProductMongo(); ObjectId obj = new ObjectId();
			  buyerMongo.set_id(obj); buyerMongo.setProductId("P10");
			  buyerMongo.setProductTitle("LAP"); buyerMongo.setProductQuantity(1000);
			  buyerMongo.setProductDesc("LAPTOP"); buyerMongo.setProductprice("$500");
			  buyerMongo.setProductImg("LAP.png"); requestParams.put("productId", "P10");
			  requestParams.put("productTitle", "LAP");
			  requestParams.put("productQuantity", "1000");
			  requestParams.put("productDesc", "LAPTOP"); requestParams.put("productprice",
			  "$500"); requestParams.put("productImg", "LAP.png");
			  request.header("Content-Type", "application/json");
			  request.body(requestParams.toJSONString()); Response response =
			  request.post("/addbuyerMongo"); int statusCode = response.getStatusCode();
			  System.out.println("#####"+statusCode); Assert.assertEquals(statusCode, 400);
			  String successCode = response.jsonPath().get("status");
			  System.out.println(successCode);
			  Assert.assertEquals("Correct Success code was returned", successCode,
			  "success");	  
	  }
	 
	
	@Test 
	public void testListProducts() {
	  System.out.println("####"+RestAssured.get("http://localhost:8080/edna/buyer/getProducts").then().assertThat().toString()); 
	}
}	
	